import {hot} from 'react-hot-loader';
/** @jsx jsx */
import {jsx, css} from '@emotion/core';

import '../images/bird.jpg';

const style = css`
  background-color: #eee;
`;

const tableData = [
  {
    first: 'Mark',
    last: 'Otto',
    handle: '@mdo'
  },
  {
    first: 'Jacob',
    last: 'Thornton',
    handle: '@fat'
  },
  {
    first: 'Larry',
    last: 'the Bird',
    handle: '@twitter'
  }
];

const carouselData = ['Slide1', 'Slide2', 'Slide3'];

class App extends PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      input1: '',
      input2: '',
      submited: false
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.onInput = this.onInput.bind(this);
  }

  onInput(event) {
    const name = event.target.getAttribute('name');
    const {value} = event.target;

    this.setState({
      [name]: value,
      submited: false
    });
  }

  onSubmit(event) {
    event.preventDefault();

    this.setState({
      submited: true
    });
  }

  render() {
    const {input1, input2, submited} = this.state;

    return (
      <React.StrictMode>
        <div className='container' css={style}>
          <h1>Hello</h1>

          <table className='table table-striped'>
            <thead className='table-dark'>
              <tr>
                <th scope='col'>#</th>
                <th scope='col'>First</th>
                <th scope='col'>Last</th>
                <th scope='col'>Handle</th>
              </tr>
            </thead>
            <tbody>
              {tableData.map((data, index) => (
                <tr key={`${data.first} ${data.last}`}>
                  <th scope='row'>{index + 1}</th>
                  <td>{data.first}</td>
                  <td>{data.last}</td>
                  <td>{data.handle}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>

        {submited && (
          <div className='container' css={style}>
            <p>Input1: {input1}</p>
            <p>Input2: {input2}</p>
          </div>
        )}

        <div className='container' css={style}>
          <form onSubmit={this.onSubmit}>
            <div className='form-group'>
              <label htmlFor='exampleInputEmail1'>Some data</label>
              <input
                type='text'
                className='form-control'
                aria-describedby='input'
                placeholder='Enter something'
                name='input1'
                value={input1}
                onChange={this.onInput}
              />
            </div>
            <div className='form-group'>
              <label htmlFor='exampleInputPassword1'>Some data 2</label>
              <input
                type='text'
                className='form-control'
                aria-describedby='input'
                placeholder='Enter something more'
                name='input2'
                value={input2}
                onChange={this.onInput}
              />
            </div>
            <button type='submit' className='btn btn-primary'>
              Submit
            </button>
          </form>
        </div>

        <div className='container' css={style}>
          <div
            id='carouselExampleControls'
            className='carousel slide'
            data-ride='carousel'>
            <div className='carousel-inner'>
              {carouselData.map((data, index) => (
                <div
                  key={data}
                  className={`carousel-item ${index === 0 ? 'active' : ''}`}>
                  <h1 className='d-block w-100 text-center'>{data}</h1>
                </div>
              ))}
            </div>
            <a
              className='carousel-control-prev'
              href='#carouselExampleControls'
              role='button'
              data-slide='prev'>
              <span className='carousel-control-prev-icon' aria-hidden='true' />
              <span className='sr-only'>Previous</span>
            </a>
            <a
              className='carousel-control-next'
              href='#carouselExampleControls'
              role='button'
              data-slide='next'>
              <span className='carousel-control-next-icon' aria-hidden='true' />
              <span className='sr-only'>Next</span>
            </a>
          </div>
        </div>

        <div className='container' css={style}>
          <img
            src='/images/bird.jpg'
            className='img-fluid'
            alt='Responsive image'
          />
        </div>
      </React.StrictMode>
    );
  }
}

export default hot(module)(App);
